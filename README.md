# Example Ansible Repository

This repo contains an example Ansible directory, playbooks and tasks for configuring and orchestrating web servers. This code has been adapted from materials learned during Creston Jamison's excellent course, [Discover Ansible](http://www.rubytreesoftware.com/courses/discover-ansible), which I highly recommend.

### General Usage

To bootstrap a new server, run the following command from the root of this
repository:

	ansible-playbook bootstrap.yml

To configure a bootstrapped server for app deployment, run the following command
from that app's directory in this repository:

```sh
cd ansible/<app-name>
ansible-playbook site.yml -i production -K --ask-vault-pass
```

To complete this command, you will need both the sudo password for your remote
user and the vault password for `<app-name>/secrets.yml`.

NOTE: Vault password for this example repo is `foobar`. Check it out, then remove the vault and create a new one with your own password:

	ansible-vault create secrets.yml
